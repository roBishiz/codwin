const express = require("express");
const app = express();
//const userController = require("./controllers/userController.js");
const hbs = require("hbs");
const homeRouter = require("./routes/homeRouter.js");

app.set("view engine", "hbs");
hbs.registerPartials(__dirname + "/views");
// определяем маршруты и их обработчики внутри роутера homeRouter
//homeRouter.get("/about", homeController.about);
app.use("/", homeRouter);
 
app.use(function (req, res, next) {
    res.status(404).send("Not Found")
});
 
app.listen(3000);